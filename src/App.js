import logo from './logo.svg';
import './App.css';
import Purifier from './blabla/dom-purifier';

function App() {
  return (
    <div className="App">
      {/* <Hookie></Hookie><br></br> */}
      <Purifier></Purifier>
    </div>
  );
}

export default App;
