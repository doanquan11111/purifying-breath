import React, { useState, useEffect } from 'react';
import { sanitize } from 'dompurify';

export default function DOMPurifier() {
    const [queryInput, setInput] = useState('');
    const [queryOutput, setOutput] = useState('');
    
    useEffect(() => {
        document.getElementById('query-input').value = queryInput;
        document.getElementById('query-output').innerHTML = queryOutput;
    });

    return (
        <div>
            <h1 className="search-form-header">Test</h1>
            <form className="search-form" autoComplete="off">
                <input className="search-input" id="query-input" type="text" name="query" />
                <button className="search-button" type='button' onClick={() => { 
                    const clean = sanitize(document.getElementById('query-input').value);
                    setInput(clean);
                    setOutput(clean);
                    }}>Sanitize</button>
            </form>
            <h3 className="search-query">Query: <span id="query-output" className="query"></span></h3>
        </div>
    )
};

