import React, { useState, useEffect } from 'react';

export default function Hookie() {
    const [count, setCount] = useState(0);

    useEffect(() => {
        document.title = `You click ${count} time(s).`;
        console.log(document.getElementById('clicked').outerHTML);
        console.log(document.getElementById('span-tag').outerHTML);
    });

    return (
        <div>
            <div id='clicked'>Haha {count}</div>
            <span id='span-tag'>Span {count}</span><br></br>
            <button onClick={() => setCount(count+1)}>
                Click me!
            </button>
        </div>
    )
};

